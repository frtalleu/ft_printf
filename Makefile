# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/11 22:42:58 by frtalleu          #+#    #+#              #
#    Updated: 2019/12/21 10:20:48 by frtalleu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	= libftprintf.a

SRCS	= libft/ft_memset.c \
		  libft/ft_bzero.c \
		  libft/ft_memcpy.c \
		  libft/ft_memccpy.c \
		  libft/ft_memmove.c \
		  libft/ft_memchr.c \
		  libft/ft_memcmp.c \
		  libft/ft_strlen.c \
		  libft/ft_isalpha.c \
		  libft/ft_isdigit.c \
		  libft/ft_isalnum.c \
		  libft/ft_isascii.c \
		  libft/ft_isprint.c \
		  libft/ft_toupper.c \
		  libft/ft_tolower.c \
		  libft/ft_strchr.c \
		  libft/ft_strrchr.c \
		  libft/ft_strncmp.c \
		  libft/ft_strlcpy.c \
		  libft/ft_strlcat.c \
		  libft/ft_strnstr.c \
		  libft/ft_atoi.c \
		  libft/ft_calloc.c \
		  libft/ft_strdup.c \
		  libft/ft_substr.c \
		  libft/ft_strjoin.c \
		  libft/ft_strtrim.c \
		  libft/ft_split.c \
		  libft/ft_itoa.c \
		  libft/ft_strmapi.c \
		  libft/ft_putchar_fd.c \
		  libft/ft_putstr_fd.c \
		  libft/ft_putendl_fd.c \
		  libft/ft_putnbr_fd.c \
		  libft/ft_split_charset.c \
		  libft/ft_word_count.c \
		  libft/ft_is_in_base.c \
		  libft/ft_lstnew_bonus.c \
		  libft/ft_lstadd_front_bonus.c \
		  libft/ft_lstsize_bonus.c \
		  libft/ft_lstlast_bonus.c \
		  libft/ft_lstadd_back_bonus.c \
		  libft/ft_lstdelone_bonus.c \
		  libft/ft_lstiter_bonus.c \
		  libft/ft_lstclear_bonus.c \
		  libft/ft_lstmap_bonus.c\
		  ft_itohexa.c \
		  ft_printf.c \
		  ft_utoa.c \
		  make_space_or_zero.c \
		  makestr.c \
		  makestruct.c \
		  strforc.c \
		  strforint.c \
		  strforstr.c \
		  ft_abs.c \
		  ft_ptoa.c \
		  onlyint.c \
		  endofint.c \

OBJS	= ${SRCS:.c=.o}

HEADER	=	libft/libft.h \
			printf.h \

CFLAGS	= -Wall -Wextra -Werror

.c.o:
			gcc ${CFLAGS} -c $< -o ${<:.c=.o}

$(NAME):	${OBJS}
			ar rc $(NAME) $(OBJS)
			ranlib $(NAME)

all:		${NAME}

clean:
				/bin/rm -f  ${OBJS}

fclean:		clean
				/bin/rm -f ${NAME}

re:			fclean all

.PHONY:		all clean fclean re
