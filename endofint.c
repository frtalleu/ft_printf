/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   endofint.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/23 23:28:58 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/23 23:29:31 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "printf.h"
#include "./libft/libft.h"

char	*cleanline(char *str, int i)
{
	int j;
	int k;

	k = 0;
	j = 0;
	if (i > 0)
		return (str);
	while (str[j] != '\0' && k <= 1)
	{
		if (str[j] == '-')
			k++;
		if (k > 1)
			str[j] = '0';
		j++;
	}
	return (str);
}
