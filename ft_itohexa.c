/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itohexa.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/18 00:36:35 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/18 00:36:36 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "./libft/libft.h"
#include "printf.h"

int		ft_sizemalloc2(unsigned int i)
{
	unsigned int	j;
	int				size;

	j = i;
	size = 0;
	while (j > 0)
	{
		size++;
		j = j / 16;
	}
	return (size);
}

char	*ft_utohexa(unsigned int i, char *base, t_flag *flags)
{
	char	*str;
	int		size;

	if (i == 0 && flags->size == 0 && flags->sizehere == 1)
		return (ft_strdup(""));
	else if (i == 0)
	{
		if (!(str = malloc(sizeof(char) * 2)))
			return (NULL);
		str[0] = base[0];
		str[1] = '\0';
		return (str);
	}
	size = ft_sizemalloc2(i);
	if (!(str = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	str[size] = '\0';
	size--;
	while (i > 0)
	{
		str[size] = base[i % 16];
		i = i / 16;
		size--;
	}
	return (str);
}
