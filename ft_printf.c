/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 04:58:56 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/09 04:58:59 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdarg.h>
#include "printf.h"
#include <stdio.h>
#include <stdlib.h>

int	checkflags(t_flag *flags)
{
	if (flags->type == 'd' || flags->type == 'i' || flags->type == 'u' ||
		flags->type == 'x' || flags->type == 'X' || flags->type == 'c' ||
		flags->type == 's' || flags->type == 'p' || flags->type == '%')
		return (1);
	return (0);
}

int	ft_printf(const char *str, ...)
{
	va_list	av;
	int		i;
	t_flag	*flags;
	int		res;

	i = 0;
	res = 0;
	va_start(av, str);
	while (str[i] != '\0')
	{
		if (str[i] == '%')
		{
			flags = fill_the_struct(&str[i], av);
			if (checkflags(flags) == 1)
				res = ft_makestr(av, flags) + res;
			i = i + flags->fsize + 1;
			free(flags);
			continue ;
		}
		write(1, &str[i], 1);
		res = res + 1;
		i++;
	}
	va_end(av);
	return (res);
}
