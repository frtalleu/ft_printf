/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ptoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/03 03:27:09 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/04 23:02:16 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "./libft/libft.h"
#include "printf.h"

int		ft_sizemalloc(long long unsigned int i)
{
	long unsigned int	j;
	int					size;

	j = i;
	size = 0;
	while (j > 0)
	{
		size++;
		j = j / 16;
	}
	return (size);
}

char	*ft_itohexa(long long unsigned int i, char *base)
{
	char	*str;
	int		size;

	if (i == 0)
	{
		if (!(str = malloc(sizeof(char) * 2)))
			return (NULL);
		str[0] = base[0];
		str[1] = '\0';
		return (str);
	}
	size = ft_sizemalloc(i);
	if (!(str = malloc(sizeof(char) * (size + 1))))
		return (NULL);
	str[size] = '\0';
	size--;
	while (i > 0)
	{
		str[size] = base[i % 16];
		i = i / 16;
		size--;
	}
	return (str);
}

char	*ft_ptoa(long long unsigned int i, t_flag *flags)
{
	char	*str;
	char	*tmp;

	if (i == 0 && flags->size == 0)
		return (ft_strdup("0x"));
	tmp = ft_itohexa(i, "0123456789abcdef");
	str = ft_strjoin("0x", tmp);
	free(tmp);
	return (str);
}
