/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_utoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/03 21:53:40 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/03 22:34:04 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include "printf.h"
#include "./libft/libft.h"

char	*ft_utoa2(unsigned int nbr, unsigned int nu, unsigned int n, int i)
{
	char *str;

	while (n != 0)
	{
		n = n / 10;
		i++;
	}
	if (!(str = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	str[i] = '\0';
	while (nu >= 10)
	{
		i--;
		str[i] = (nu % 10) + 48;
		nu = nu / 10;
	}
	i--;
	if (nbr != 0)
		str[i] = nu + 48;
	return (str);
}

char	*ft_utoa(unsigned int n, t_flag *flags)
{
	unsigned int	nu;
	unsigned int	nbr;
	int				i;

	if (n == 0 && flags->size == 0 && flags->sizehere == 1)
		return (ft_strdup(""));
	i = 0;
	nu = n;
	nbr = nu;
	return (ft_utoa2(nbr, nu, n, i));
}
