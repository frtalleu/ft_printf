/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_space_or_zero.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/06 00:24:04 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/06 00:26:08 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "printf.h"
#include "./libft/libft.h"

char	*ft_strzero(int i)
{
	char	*str;

	if (i <= 0)
	{
		if ((str = ft_strdup("")))
			return (str);
		return (NULL);
	}
	if (!(str = ft_calloc(sizeof(char), (i + 1))))
		return (NULL);
	i--;
	while (i >= 0)
	{
		str[i] = '0';
		i--;
	}
	return (str);
}

char	*ft_strspace(int i)
{
	char	*str;

	if (i <= 0)
	{
		if ((str = ft_strdup("")))
			return (str);
		return (NULL);
	}
	if (!(str = ft_calloc(sizeof(char), (i + 1))))
		return (NULL);
	i--;
	while (i >= 0)
	{
		str[i] = ' ';
		i--;
	}
	return (str);
}
