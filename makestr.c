/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makestr.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/10 02:27:55 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/10 02:27:57 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>
#include "printf.h"
#include "./libft/libft.h"
#include <stdio.h>

void	ft_printstr(t_flag *flags, char *str, int len)
{
	if (flags->type != 'c' && flags->type != '%')
		write(1, str, len);
}

int		ft_makestr(va_list av, t_flag *flags)
{
	char	*str;
	int		len;

	if (flags->type == 'd' || flags->type == 'i' || flags->type == 'u' ||
			flags->type == 'x' || flags->type == 'X' || flags->type == 'p')
	{
		str = strforint(flags, av);
		len = ft_strlen(str);
	}
	if (flags->type == 's')
		str = strforstr(av, flags);
	if (flags->type == 's')
		len = ft_strlen(str);
	if (flags->type == 'c' || flags->type == '%')
	{
		strforc(flags, av);
		if (ft_abs(flags->width) > 1)
			len = ft_abs(flags->width);
		else
			len = 1;
	}
	ft_printstr(flags, str, len);
	if (flags->type != 'c' && flags->type != '%' && (unsigned int)&str != 0)
		free(str);
	return (len);
}
