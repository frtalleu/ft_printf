/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   makestruct.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/09 01:06:15 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/09 01:06:17 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "printf.h"
#include "./libft/libft.h"

t_flag	*finish_the_struct(t_flag *flags, const char *str, int i, va_list av)
{
	if (str[i] == '*')
	{
		flags->width = va_arg(av, int);
		i++;
	}
	flags->size = 0;
	if (str[i] == '.')
		flags->sizehere = 1;
	if (str[i] == '.')
		i++;
	else
		flags->size = -1;
	while (str[i] >= 48 && str[i] <= 57)
	{
		flags->size = flags->size * 10 + (str[i] - 48);
		i++;
	}
	if (str[i] == '*')
		flags->size = va_arg(av, int);
	if (str[i] == '*')
		i++;
	flags->type = str[i];
	flags->fsize = i;
	flags->sizeher = flags->sizehere;
	return (flags);
}

t_flag	*fill_the_struct(const char *str, va_list va)
{
	t_flag	*flags;
	int		i;

	i = 1;
	if (!(flags = malloc(sizeof(t_flag))))
		return (NULL);
	flags->flag = 0;
	flags->sizehere = -1;
	while (str[i] == '0' || str[i] == '-')
	{
		if (flags->flag != '-')
			flags->flag = str[i];
		i++;
	}
	flags->width = 0;
	while (str[i] >= 48 && str[i] <= 57)
	{
		flags->width = flags->width * 10 + (str[i] - 48);
		i++;
	}
	return (finish_the_struct(flags, str, i, va));
}
