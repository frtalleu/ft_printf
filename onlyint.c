/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   onlyint.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/19 01:28:52 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/23 10:35:50 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdarg.h>
#include "printf.h"
#include "./libft/libft.h"
#include <stdio.h>
#include <unistd.h>

int		ft_checkline(char *str)
{
	int				i;

	i = 0;
	if (!str || str[i] == '\0')
		return (-1);
	while (str[i] != '\0')
	{
		if (str[i] == '-')
			return (i);
		i++;
	}
	return (-1);
}

char	*onlyint3(char *str, t_flag *flags, int i)
{
	int j;

	j = 0;
	if (ft_abs(flags->width) > ft_strlen(str))
	{
		str = strforint2(flags, str);
		if (str[0] == '0' && i == -1)
			str[0] = '-';
		else if (ft_checkline(str) == -1 && i == -1)
		{
			while (ft_isdigit(str[j + 1]) != 1)
				j++;
			str[j] = '-';
		}
	}
	return (cleanline(str, i));
}

char	*onlyint2(char *str, int i, t_flag *flags)
{
	char	*tmp;
	char	*st;

	if (ft_abs(flags->size) > ft_strlen(str))
	{
		if (!(tmp = ft_strzero(flags->size - ft_strlen(str))))
			return (NULL);
		if (!(st = ft_strjoin(tmp, str)))
			return (NULL);
		free(str);
		free(tmp);
		str = st;
		if (i == -1 && str[0] != '-')
		{
			if (!(tmp = ft_strjoin("-", str)))
				return (NULL);
			free(str);
			str = tmp;
		}
	}
	return (onlyint3(str, flags, i));
}

char	*intisneg(char *str)
{
	char	*st;

	if (!(st = ft_strjoin("-", str)))
		return (NULL);
	free(str);
	return (st);
}

char	*onlyint(t_flag *flags, va_list va)
{
	int				i;
	unsigned int	j;
	char			*str;

	i = va_arg(va, int);
	if (i < 0)
		j = -i;
	if (i < 0)
		i = -1;
	else
	{
		j = i;
		i = 1;
	}
	if (j == 0 && flags->size != 0 && !(str = ft_strdup("0")))
		return (NULL);
	if ((j != 0 || flags->size == 0) && !(str = ft_utoa(j, flags)))
		return (NULL);
	if (i == -1 && (flags->size < 0 || ft_abs(flags->size) <= ft_strlen(str))
		&& (ft_abs(flags->width) < (ft_strlen(str) + ft_abs(flags->size)) ||
		flags->flag != '0' || flags->width < 0 || flags->sizehere == 1) &&
		!(flags->flag == '0' && flags->width > 0
		&& ft_abs(flags->width) > ft_strlen(str)) && !(str = intisneg(str)))
		return (NULL);
	return (onlyint2(str, i, flags));
}
