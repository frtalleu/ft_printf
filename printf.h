/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 02:55:56 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/23 10:36:09 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_H
# define PRINTF_H
# include <stdarg.h>
# include "./libft/libft.h"

typedef struct	s_flag
{
	int		sizeher;
	int		sizehere;
	char	flag;
	int		width;
	int		size;
	char	type;
	int		fsize;
}				t_flag;

typedef struct	s_iands
{
	char	*str;
	int		len;
}				t_iands;

char			*cleanline(char *str, int i);
void			ft_printstr(t_flag *flags, char *str, int len);
int				checkflags(t_flag *flags);
char			*ft_strzero(int i);
char			*ft_strspace(int i);
char			*ft_ptoa(long long unsigned int i, t_flag *flags);
char			*ft_itohexa(long long unsigned int i, char *base);
int				ft_sizemalloc(long long unsigned int i);
int				ft_printf(const char *str, ...);
char			*ft_utoa(unsigned int n, t_flag *flags);
char			*ft_utoa2(unsigned int nbr, unsigned int nu, unsigned int n
				, int i);
int				ft_makestr(va_list av, t_flag *flags);
t_flag			*finish_the_struct(t_flag *flags, const char *str, int i
				, va_list av);
t_flag			*fill_the_struct(const char *str, va_list va);
void			strforc(t_flag	*flags, va_list av);
char			*strforstr(va_list va, t_flag *flags);
char			*ft_make_dup(va_list va, t_flag *flags);
char			*strforstr2(char *str, t_flag *flags, unsigned int i);
char			*manage_nu(t_flag *flags, va_list va);
char			*strforint2(t_flag *flags, char *str);
char			*strforint(t_flag *flags, va_list va);
unsigned int	ft_abs(int i);
char			*ft_utohexa(unsigned int i, char *base, t_flag *flags);
int				ft_sizemalloc2(unsigned int i);
char			*onlyint(t_flag *flags, va_list va);
char			*intisneg(char *str);
char			*onlyint2(char *str, int i, t_flag *flags);
char			*onlyint3(char *str, t_flag *flags, int i);
#endif
