/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strforc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 00:28:59 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/12 00:29:02 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "printf.h"
#include "./libft/libft.h"
#include <stdio.h>
#include <unistd.h>

void	strforc(t_flag *flags, va_list av)
{
	unsigned int i;

	i = 0;
	if (flags->width <= 0 || flags->flag == '-')
		if (flags->type == '%')
			write(1, "%", 1);
	if (flags->width <= 0 || flags->flag == '-')
		if (flags->type == 'c')
			ft_putchar_fd(va_arg(av, int), 1);
	if (flags->flag == '0' && flags->type == '%' && flags->width > 0 &&
		ft_abs(flags->width) > 1)
		while (i++ < ft_abs(flags->width) - 1)
			write(1, "0", 1);
	else if (ft_abs(flags->width) > 1)
		while (i++ < ft_abs(flags->width) - 1)
			write(1, " ", 1);
	if (flags->width > 0 && flags->flag != '-')
		if (flags->type == '%')
			write(1, "%", 1);
	if (flags->width > 0 && flags->flag != '-')
		if (flags->type == 'c')
			ft_putchar_fd(va_arg(av, int), 1);
}
