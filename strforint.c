/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strforint.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/15 04:07:08 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/23 10:32:23 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdarg.h>
#include "printf.h"
#include "./libft/libft.h"
#include <stdio.h>
#include <unistd.h>

char	*manage_nu(t_flag *flags, va_list va)
{
	char			*str;
	unsigned int	i;

	if (flags->type == 'u')
	{
		i = va_arg(va, unsigned int);
		if ((flags->sizehere == -1 || flags->size != 0) && i == 0)
			return (ft_strdup("0"));
		else if ((str = ft_utoa(i, flags)))
			return (str);
	}
	if (flags->type == 'x')
		if ((str = ft_utohexa(va_arg(va, int), "0123456789abcdef", flags)))
			return (str);
	if (flags->type == 'X')
		if ((str = ft_utohexa(va_arg(va, int), "0123456789ABCDEF", flags)))
			return (str);
	if (flags->type == 'p')
		if ((str = ft_ptoa(va_arg(va, unsigned long long), flags)))
			return (str);
	return (NULL);
}

char	*strforint2(t_flag *flags, char *str)
{
	char	*tmp;
	char	*st;

	if (ft_abs(flags->width) > ft_strlen(str))
	{
		if (flags->flag == '0' && (flags->sizehere == -1 || flags->size < 0)
			&& flags->width > 0)
		{
			if (!(tmp = ft_strzero(flags->width - ft_strlen(str))))
				return (NULL);
		}
		else if (flags->flag != '0' || flags->sizeher == 1 || flags->width <= 0)
			if (!(tmp = ft_strspace(ft_abs(flags->width) - ft_strlen(str))))
				return (NULL);
		if (flags->flag == '-' || flags->width < 0)
			if (!(st = ft_strjoin(str, tmp)))
				return (NULL);
		if (flags->flag != '-' && flags->width > 0)
			if (!(st = ft_strjoin(tmp, str)))
				return (NULL);
		free(tmp);
		free(str);
		str = st;
	}
	return (str);
}

char	*strforint(t_flag *flags, va_list va)
{
	char	*str;
	char	*tmp;
	int		i;
	char	*st;

	if (flags->type == 'i' || flags->type == 'd')
		return (onlyint(flags, va));
	if (!(str = manage_nu(flags, va)))
		return (NULL);
	i = ft_strlen(str);
	if (flags->size > i)
	{
		if (!(tmp = ft_strzero(flags->size - i)))
			return (NULL);
		if (!(st = ft_strjoin(tmp, str)))
			return (NULL);
		free(str);
		free(tmp);
		str = st;
	}
	return (strforint2(flags, str));
}
