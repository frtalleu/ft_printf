/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strforstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: frtalleu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/12 04:19:21 by frtalleu          #+#    #+#             */
/*   Updated: 2019/12/12 04:19:24 by frtalleu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "printf.h"
#include "./libft/libft.h"
#include <stdio.h>

char	*strforstr2(char *str, t_flag *flags, unsigned int i)
{
	char	*tmp;
	char	*st;

	if (i < ft_abs(flags->width))
	{
		if (flags->flag == '0' && ft_abs(flags->width) > i && flags->width > 0)
			if (!(tmp = ft_strzero(flags->width - i)))
				return (NULL);
		if (flags->flag != '0' && ft_abs(flags->width) > i)
			if (!(tmp = ft_strspace(ft_abs(flags->width) - i)))
				return (NULL);
		if (flags->flag == '-' || flags->width < 0)
			if (!(st = ft_strjoin(str, tmp)))
				return (NULL);
		if (flags->flag != '-' && flags->width > 0)
			if (!(st = ft_strjoin(tmp, str)))
				return (NULL);
		free(str);
		free(tmp);
		return (st);
	}
	return (str);
}

char	*ft_make_dup(va_list va, t_flag *flags)
{
	char	*tmp;
	char	*str;
	char	*st;

	tmp = va_arg(va, char *);
	if (tmp == NULL)
		if (!(str = ft_strdup("(null)")))
			return (NULL);
	if (tmp != NULL)
		if (!(str = ft_strdup(tmp)))
			return (NULL);
	if (ft_abs(flags->size) < ft_strlen(str) && flags->sizehere == 1)
		str[flags->size] = 0;
	if (!(st = ft_strdup(str)))
		return (NULL);
	free(str);
	str = st;
	return (st);
}

char	*strforstr(va_list va, t_flag *flags)
{
	char				*str;
	unsigned int		i;

	if (!(str = ft_make_dup(va, flags)))
		return (NULL);
	i = ft_strlen(str);
	return (strforstr2(str, flags, i));
}
